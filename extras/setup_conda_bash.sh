#! /usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage: ./setup_conda_bash.sh mac_or_linux add_numeric_yes_or_no"
    echo "Choose 'mac' or 'linux' as first argument"
    echo "Choose 'yes' or 'no' to add a few numerial packages"
    exit
fi

os=$1
add_numeric=$2

if [[ ${add_numeric} != "yes" ]] && [[ ${add_numeric} != "no" ]] ; then
    echo "Please write 'yes' or write 'no' as an argument as second argument."
    exit
fi

if [[ ${os} != "mac" ]] && [[ ${os} != "linux" ]] ; then
    echo "Please write 'mac' for the MacOS binaries  and 'linux' for Linux."
    exit
fi

if [ ${os} == 'mac' ]; then
    script_url="https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh"
else
    script_url="https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"
fi

script_name=${script_url##*/}

#download 
curl -L -s ${script_url} --output ${script_name}

if [ ! -e  ${script_name} ]; then
    echo "${script_name} is not here"
    echo "If you don't have curl, try changing this script to use wget"
    exit
fi

bash ${script_name} -b -p $HOME/miniconda3

echo "Should be done, removing script"
rm -fr ${script_name}

echo "Adding conda shell to ${HOME}/.bashrc"
echo 'eval "$($HOME/miniconda3/bin/conda shell.bash hook)"' >> ${HOME}/.bashrc
eval "$($HOME/miniconda3/bin/conda shell.bash hook)"

if [ ${add_numeric} == 'yes' ]; then
    programs="jupyter scipy pandas scikit-learn  matplotlib altair jupyterlab"
    echo "Create a new environment for py37 with the following libraries:"
    echo ${programs}
    conda create -n py37 python=3.7 ${programs} -y -c conda-forge
fi
echo "All done!"

