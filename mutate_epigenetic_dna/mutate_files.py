#! /usr/bin/env python

import contextlib
import logging
import os
import shutil
import subprocess
from pathlib import Path
from typing import List, Tuple

from mutate_epigenetic_dna.parsing import Input
from mutate_epigenetic_dna.prepare_multi_TI_mut_py3 import mutate_api

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


@contextlib.contextmanager
def cd(path):
    old_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_path)


def clean_up(list_f):
    for f in list_f:
        Path(f).unlink()


def clean_comments(lines, comment=";"):
    """Remove comments from top"""
    ret = []
    for line in lines:
        if comment in line:
            idx = line.index(comment)
            new_line = line[:idx].strip()
            if new_line:
                ret.append(new_line)
        else:
            new_line = line.strip()
            if new_line:
                ret.append(new_line)
    return ret


def read_top_section(lines, section):
    """Return array of strings, each a line in the section """
    ret = []
    end = "["
    for i, line in enumerate(lines):
        if line.strip() == f"[ {section} ]":
            for line2 in lines[i + 1:]:
                if not end in line2:
                    ret.append(line2)
                else:
                    break
    return ret


def write_top_section(fp, section, msgs):
    print(f"[ {section} ]", file=fp)
    for line in msgs:
        print(f"{line}", file=fp)
    print("", file=fp)


def top_to_itp(inp_top, out_itp):
    """Change from top to itp"""

    with open(inp_top) as fi:
        dirty_lines = fi.readlines()

    lines = clean_comments(dirty_lines, ";")
    sections = ["atomtypes", "moleculetype", "atoms", "bonds", "pairs",
                "angles", "dihedrals"]
    with open(out_itp, "w") as fo:
        for section in sections:
            x = read_top_section(lines, section)
            write_top_section(fo, section, x)


def prepare_tleap_string(*, header: Path = None, pdb: Path = None,
                         prep: Path = None, ff_path: Path = None) -> Tuple[
    Path, List[str]]:
    """Write the tleap input"""
    # read in the templated header
    tleap_in_folder = pdb.parent / Path("tleap.inp")
    shutil.copy(header, tleap_in_folder)
    copied_files = []
    for f in ff_path.iterdir():
        if f.is_file():
            shutil.copy(f, pdb.parent)
            copied_files.append(f.name)

    with open(tleap_in_folder, "a") as fo:
        msg = f"loadamberprep  {str(prep)}\n"
        msg += f"unit = loadpdb {pdb.name}\n"
        msg += f"saveamberparm unit amb.top amb.crd\n"
        msg += "quit"
        print(msg, file=fo)
    return tleap_in_folder, copied_files


def print_error(msg: str, *, mutation: str = None,
                program: str = None) -> None:
    """Print stoud if subproccess run did not work"""
    logging.error("-------------------")
    logging.error(f"{program} did not work for {mutation}")
    logging.error("Here is the output:")
    for line in msg.split('\n'):
        logging.error(line)
    logging.error("-------------------\n")


def prepare_itps(args, clean=True):
    """Prepare all the itps required for the mutations"""

    header = args.ff_folder / args.tleap_header
    list_ok_itp = {}

    for epi, file in args.pdb.items():
        logging.info(f"Working on {epi}")
        prep = args.prep_files[epi]
        ff_path = args.ff_folder

        inp_tleap, list_f = prepare_tleap_string(header=header,
                                                 pdb=file,
                                                 prep=prep,
                                                 ff_path=ff_path)
        with cd(file.parent):
            ret = subprocess.run(["tleap", "-f", str(inp_tleap.name)],
                                 stdout=subprocess.PIPE, encoding='utf-8')
            # even if it fails in producing a topology tleap won't return
            # a code different than 0, so I have to specifically check stdout
            if "Parameter file was not saved" in ret.stdout:
                print_error(ret.stdout, mutation=epi, program="tleap")
                continue

            ret = subprocess.run(
                ['amb2gmx_dihe.pl', "--prmtop", "amb.top", "--crd", "amb.crd",
                 "--outname", "amb_GMX"],
                stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                encoding='utf-8')
            if ret.returncode != 0:
                print_error(ret.stdout, mutation=epi,
                            program="amb2gmx_dihe.pl")

                continue
            top_to_itp("amb_GMX.top", f"{epi}.itp")

            # clean up a bit
            if clean:
                clean_up(list_f + ["tleap.inp"])
                clean_amb2gmx = ['amb_GMX.gro', 'amb_GMX.top']
                clean_up(clean_amb2gmx)
                clean_tleap = ["amb.top", "amb.crd", "leap.log"]
                clean_up(clean_tleap)

            # we should be done processing here

            list_ok_itp.setdefault(epi, str(file.parent) + f"/{epi}.itp")

    return list_ok_itp


def run_mutations(args: Input) -> None:
    """Runs the mutations for each case

    :return: None
    """

    dict_itps = prepare_itps(args)

    for mut in args.mutations:
        big, small = mut.split("_")
        mut_type = "2".join([big, small])
        mutate_api(ilarge=dict_itps[big],
                   ismall=dict_itps[small],
                   mut_residues=args.mut_resid,
                   mut_type=mut_type,
                   out_itp=f"ti_topology_{mut_type}.itp",
                   )

    logging.info("All done!")
