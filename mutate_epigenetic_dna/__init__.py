__version__ = '0.1.1'

from .mutate_epigenetic import start_mutations
from .prepare_multi_TI_mut_py3 import run_mutate
