#!/usr/bin/env python
import sys

from .cmdline import parse_cmd_line

py_version = sys.version_info.major + 0.1 * sys.version_info.minor

SUPPORTED_PYTHON = 3.5

if py_version < SUPPORTED_PYTHON:
    print(
        "Sorry, you must use python {0:.1f} or above".format(SUPPORTED_PYTHON))
    sys.exit()


def start_mutations():
    sys.exit(parse_cmd_line())


if __name__ == '__main__':
    sys.exit(parse_cmd_line())
