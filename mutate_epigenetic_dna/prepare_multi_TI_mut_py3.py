#! /usr/bin/env python3

import argparse
import logging.config
import sys
from typing import List

from . import itp_reader

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

__doc__ = """
Functionality: prepare a mixed topology to perform
TI in gromacs. It is meant to work only on small modifications
(functional groups, maily) and I assume it will fail for large ones.
To use it, you need to feed two itp files with all the ff terms
explicitely listed. The best way is to use amb2gmx_diheRB.pl
script. AFAIR, gmx had some issues doing TI with terms of different
multiplicity, so i specifically need RB for the dihedrals that need
to change.
The first topology should *always* be the one with more atoms.

IMPORTANT: the dictionary mut_pairs lists the correspondence
of atoms that mutate. Make sure you have it properly defined.

This is a port of my original script written for python2.7 + pmx library
The pmx library has not been ported to python3, which is a pitty.

I use here a modified topology reader from GromacsWrapper, and not GromacsWrapper
directly, because I had to modify it a bit in order to get it to work, and I 
could not be bother to send an issue. There is a lot of code there I don't need, 
but it's tedious to clean it up. The API is quite horrible and not very 
well documented, but better than having to parse by hand. 
"""

MUT_PAIRS_mC_TO_C = {"C55": "H5", "H51": "DUM", "H52": "DUM", "H53": "DUM"}
MUT_PAIRS_hmC_TO_mC = {
    "C55": "C55",
    "H51": "H51",
    "H52": "H52",
    "OH5": "H53",
    "HO5": "DUM",
}

MUTATION = {"mC2C": MUT_PAIRS_mC_TO_C, "hmC2mC": MUT_PAIRS_hmC_TO_mC}

Description = "\n \
Functionality: prepare a mixed topology to perform \n\
TI in gromacs. It is meant to work only on small modifications \n \
(functional groups, mainly) and I assume it will fail for large ones.\n \
To use it, you need to feed two itp files with all the FF terms \n \
explicitely listed. The best way is to use amb2gmx_dihe.pl \n \
script. AFAIR, gmx had some issues doing TI with terms of different  \n \
multiplicity, so i specifically need RB for the dihedrals that need \n \
to change.\n \
The first topology should *always* be the one with more atoms. ----------- \n \
\n \
IMPORTANT NOTE: make sure that the atomtypes defined in the mutation\n \
dictionary, hardcoded here, make sense. \n \
\n \
\n \
"
whowhen = "G. Portella, v1.2, 07-06-2019"


class Mutator(object):
    """Main class to mutate small functional groups"""

    def __init__(self, args):
        """Load the topologies"""

        self.top_l = itp_reader.TOP(args["il"][0])
        self.top_s = itp_reader.TOP(args["is"][0])
        self.out_itp = args["out_itp"]
        self.mut_pairs = MUTATION[args["mut_type"][0]]
        self.mut_res_num = args["r"]

        self.fp = None
        self.id_mut = {}  # getps populated in mutate_atoms...
        # which measn you can't  mutate_bonds before
        # not very nice style, but it's all private anyway
        self.map_all = {}
        self.at_l = {}

        mol_name_l = next(iter(self.top_l.dict_molname_mol.keys()))
        mol_name_s = next(iter(self.top_s.dict_molname_mol.keys()))

        if mol_name_l != mol_name_s:
            logging.warning(
                "The molecule name of input small and large is different!"
            )

        self.pairs_l = self.top_l.dict_molname_mol[mol_name_l].pairs
        self.pairs_s = self.top_s.dict_molname_mol[mol_name_s].pairs

        self.atoms_l = self.top_l.dict_molname_mol[mol_name_l].atoms
        self.atoms_s = self.top_s.dict_molname_mol[mol_name_s].atoms
        # in case ipt numbers don't start at 1
        self.str_at = self.atoms_l[0].number

        self.bonds_l = self.top_l.dict_molname_mol[mol_name_l].bonds
        self.bonds_s = self.top_s.dict_molname_mol[mol_name_s].bonds

        self.angles_l = self.top_l.dict_molname_mol[mol_name_l].angles
        self.angles_s = self.top_s.dict_molname_mol[mol_name_s].angles

        self.dihedrals_l = self.top_l.dict_molname_mol[mol_name_l].dihedrals
        self.dihedrals_s = self.top_s.dict_molname_mol[mol_name_s].dihedrals

    def __prepare_atomtypes(self):
        """
        Reads the atomtypes section from the itps, checks wich ones are needed
        from the itp with less atoms (second one) and puts in at1 dictionary.
        The key of the dictionaries are the atomtypes, and the values are the
        bonded atomtypes and rest of the atomtypes entry, in a list of strings.
        It includes the DUM definition by default.
        """
        at_s = {}
        for at in self.top_l.atomtypes:
            self.at_l[at.atype] = at
        for at in self.top_s.atomtypes:
            at_s[at.atype] = at
        for a2 in at_s:
            if a2 not in self.at_l:
                self.at_l[a2] = at_s[a2]
        # Finally we add the dummy definition, regardless.
        dum = itp_reader.blocks.AtomType("gromacs")
        dum.atype = "DUM"
        dum.atnum = 0
        dum.mass = 0
        dum.charge = 0
        dum.bond_type = "DUM"
        dum.gromacs = {"param": {"lje": 0, "ljl": 0, "lje14": 0, "ljl14": 0}}
        self.at_l["DUM"] = dum

        print("[ atomtypes ]", file=self.fp)
        header = "; name  bond_type  mass  charge   ptype    sigma   epsilon"
        print(header, file=self.fp)
        for a, at in self.at_l.items():
            msg = f"{a:>4s} {a:>4s}  0.000  0.000   A  "
            msg += f"{at.gromacs['param']['lje']:>11.6e} "
            msg += f"{at.gromacs['param']['ljl']:>11.4e}"
            print(msg, file=self.fp)

        print("\n", file=self.fp)

    def __mutate_atoms(self):

        # this is a bit backwards, I guess this api is not
        # designed for human beings

        print("[ atoms ]", file=self.fp)
        for a_l in self.atoms_l:
            if a_l.resnumb not in self.mut_res_num:
                msg = f"{a_l.number:6d} {a_l.atomtype:>11s}{a_l.resnumb:>7d}"
                msg += f"{a_l.resname:>7s}{a_l.name:>7s} {a_l.number:>7d}"
                msg += f"{a_l.charge:>11.6f} {a_l.mass:>11.4f}"
                print(msg, file=self.fp)
            else:
                # check if we need to mutate it
                # and get the name of the final state
                # if it's a dummy, put dummy type and zero charge
                if a_l.name in self.mut_pairs:
                    B_name = self.mut_pairs[a_l.name]
                else:
                    B_name = a_l.name
                if B_name == "DUM":
                    extra = "DUM", 0.000_000, a_l.mass
                    found = True
                else:
                    for a_s in self.atoms_s:
                        if a_s.resnumb == a_l.resnumb and B_name == a_s.name:
                            extra = a_s.atomtype, a_s.charge, a_s.mass
                            found = True
                            # we don't add the dummy atoms, as we will
                            # not touch the bonded terms
                            if a_l.name in self.mut_pairs:
                                self.id_mut[a_l.number] = a_s.number
                if found:
                    atB = extra[0]
                    qB = extra[1]
                    mB = extra[2]
                    msg = (
                        f"{a_l.number:6d} {a_l.atomtype:>11s}{a_l.resnumb:>7d}"
                    )

                    msg += f"{a_l.resname:>7s}{a_l.name:>7s} {a_l.number:>7d}"
                    msg += f"{a_l.charge:>11.6f} {a_l.mass:>11.4f} "
                    msg += f"{atB:>11s} {qB:>11.6f} {mB:>11.4f} ; TI atoms"
                    print(msg, file=self.fp)
                    found = False
                else:
                    msg = "ERROR! I could not find one atomtype in the first "
                    msg += "itp file, and it was also not considered a dummy."
                    logging.error(msg)
                    sys.exit(1)
        print("", file=self.fp)

    def __write_moltype(self):
        """We will take the name of the topology with largest num atoms"""

        mol_name_l = next(iter(self.top_l.dict_molname_mol.keys()))
        excl = self.top_l.dict_molname_mol[mol_name_l].exclusion_numb
        print("[ moleculetype ]", file=self.fp)
        print("; Name    nrexcl", file=self.fp)
        print(f"{mol_name_l}  {excl}\n", file=self.fp)

    def __do_built_map_all(self):
        """
        Map the old and new atomtypes, the key is the atomtypes
        of the topology 1, the one with more atoms.
        """
        for a_l in self.atoms_l:
            if a_l.name in self.mut_pairs:
                B_name = self.mut_pairs[a_l.name]
            else:
                B_name = a_l.name

            for a_s in self.atoms_s:
                if a_s.resnumb == a_l.resnumb and B_name == a_s.name:
                    self.map_all[a_l.number] = a_s.number

    def __do_pairs(self):
        """The list of pairs is the same as the one in the larger itp"""

        print("[ pairs ]", file=self.fp)
        print(";  ai    aj       funct", file=self.fp)
        for p_l in self.pairs_l:
            fu = p_l.gromacs["func"]
            print(
                f"{p_l.atom1.number:>7d} {p_l.atom2.number:>7d}      {fu:>2d}",
                file=self.fp,
            )

    def __mutate_bonds(self):
        """
        We only modify values for pairs that are changing
        the ones involving dummies are left the same
        we check the dictionary for equivalence, but we need to make
        the entry exists, as it could be one atom whose type does not change
        but is involved in a bond with one that does change.
        """

        map_all = self.map_all
        print("\n [ bonds ]", file=self.fp)
        dum = False

        for b_l in self.bonds_l:
            if (
                    b_l.atom1.number in self.id_mut
                    or b_l.atom2.number in self.id_mut
            ):
                id_s = []
                for at_bonds in [b_l.atom1, b_l.atom2]:
                    if at_bonds.number in map_all:
                        id_s.append(map_all[at_bonds.number])
                        name_b1 = self.atoms_l[
                            at_bonds.number - self.str_at
                            ].name
                    else:
                        # it must be the dummy, but we check just in case
                        name_b1 = self.atoms_l[
                            at_bonds.number - self.str_at
                            ].name
                        if name_b1 in self.mut_pairs:
                            if self.mut_pairs[name_b1] == "DUM":
                                dum = True
                            else:
                                logging.warning(
                                    f"{name_b1} not mapped or a dummy"
                                )
                        else:
                            print(f"{name_b1} not mapped or a dummy")
                if dum:
                    self.__print_bonds_single(b_l)
                else:
                    for b_s in self.bonds_s:
                        if (
                                id_s[0] == b_s.atom1.number
                                and id_s[1] == b_s.atom2.number
                        ) or (
                                id_s[1] == b_s.atom1.number
                                and id_s[0] == b_s.atom2.number
                        ):
                            self.__print_bonds_TI(b_l, b_s)
                dum = False
            else:
                self.__print_bonds_single(b_l)

    def __print_bonds_single(self, b):
        """Helper function print angles"""
        func = b.gromacs["func"]
        kb = b.gromacs["param"]["kb"]
        b0 = b.gromacs["param"]["b0"]
        msg = f"{b.atom1.number:>6d} "
        msg += f"{b.atom2.number:>6d} "
        msg += f"{func:>4d}  {b0:>14.6f} {kb:>14.6f} "
        print(msg, file=self.fp)

    def __print_bonds_TI(self, b1, b2):
        """Helper function print angles"""
        func = b1.gromacs["func"]
        msg = f"{b1.atom1.number:>6d} "
        msg += f"{b1.atom2.number:>6d} "
        msg += f" {func:>4d}  "
        for b in [b1, b2]:
            kb = b.gromacs["param"]["kb"]
            b0 = b.gromacs["param"]["b0"]
            msg += f"{b0:>14.6f} {kb:>14.6f} "
        msg += " ; TI bonds"
        print(msg, file=self.fp)

    @staticmethod
    def __check_None(testers, where):
        if None in testers:
            logging.error(f"Lacking explicit FF terms in {where}")
            sys.exit()

    def __do_angles(self):
        """ Mutate angles """

        map_all = self.map_all
        print("\n [ angles ]", file=self.fp)
        dum = False

        for a_l in self.angles_l:
            if (
                    a_l.atom1.number in self.id_mut
                    or a_l.atom2.number in self.id_mut
                    or a_l.atom3.number in self.id_mut
            ):
                id_s = []
                for at_ang in [a_l.atom1, a_l.atom2, a_l.atom3]:
                    if at_ang.number in map_all:
                        id_s.append(map_all[at_ang.number])
                    else:
                        # must be dummy, but check
                        name_b1 = self.atoms_l[
                            at_ang.number - self.str_at
                            ].name
                        if name_b1 in self.mut_pairs:
                            if self.mut_pairs[name_b1] == "DUM":
                                dum = True
                            else:
                                logging.warning(
                                    f"{name_b1} not mapped or a dummy"
                                )
                        else:
                            print(f"{name_b1} not mapped or a dummy")
                if dum:
                    self.__print_angles_single(a_l)
                else:
                    for a_s in self.angles_s:
                        if (
                                id_s[0] == a_s.atom1.number
                                and id_s[1] == a_s.atom2.number
                                and id_s[2] == a_s.atom3.number
                        ) or (
                                id_s[2] == a_s.atom1.number
                                and id_s[1] == a_s.atom2.number
                                and id_s[0] == a_s.atom3.number
                        ):
                            self.__print_angles_TI(a_l, a_s)
                dum = False
            else:
                self.__print_angles_single(a_l)

    def __print_angles_single(self, a):
        """Helper function print angles"""
        func = a.gromacs["func"]
        kb = a.gromacs["param"]["ktetha"]
        b0 = a.gromacs["param"]["tetha0"]
        self.__check_None([kb, b0], "[ angles ]")
        msg = f"{a.atom1.number:>6d} "
        msg += f"{a.atom2.number:>6d} "
        msg += f"{a.atom3.number:>6d} "
        msg += f"{func:>4d}  {b0:>14.6f} {kb:>14.6f} "
        print(msg, file=self.fp)

    def __print_angles_TI(self, a1, a2):
        """Helper function print angles"""
        func = a1.gromacs["func"]
        msg = f"{a1.atom1.number:>6d} "
        msg += f"{a1.atom2.number:>6d} "
        msg += f"{a1.atom3.number:>6d} "
        msg += f" {func:>4d}  "
        for a in [a1, a2]:
            kb = a.gromacs["param"]["ktetha"]
            b0 = a.gromacs["param"]["tetha0"]
            self.__check_None([kb, b0], "[ angles ]")
            msg += f"{b0:>14.6f} {kb:>14.6f} "
        msg += " ; TI angles"
        print(msg, file=self.fp)

    def __print_dih1_single(self, d):
        """Helper function"""
        delta = d.gromacs["param"]["delta"]
        kchi = d.gromacs["param"]["kchi"]
        n = d.gromacs["param"]["n"]
        msg = f"{d.atom1.number:>6d} "
        msg += f"{d.atom2.number:>6d} "
        msg += f"{d.atom3.number:>6d} "
        msg += f"{d.atom4.number:>6d}   1"
        msg += f"{delta:12.6f} {kchi:12.6f} {n:6d}"
        print(msg, file=self.fp)

    def __print_dih1_TI(self, d1, d2):
        """Helper function print dihedral ft 1
        Requires two DihedralType objects
        """
        msg = f"{d1.atom1.number:>6d} "
        msg += f"{d1.atom2.number:>6d} "
        msg += f"{d1.atom3.number:>6d} "
        msg += f"{d1.atom4.number:>6d}   1"
        for d in [d1, d2]:
            delta = d.gromacs["param"]["delta"]
            kchi = d.gromacs["param"]["kchi"]
            n = d.gromacs["param"]["n"]
            msg += f"{delta:12.6f} {kchi:12.6f} {n:6d}"
        msg += " ; TI dihedrals"
        print(msg, file=self.fp)

    def __print_dih3_single(self, d):
        """Helper function print dihedral ft 3"""

        msg = f"{d.atom1.number:>6d} "
        msg += f"{d.atom2.number:>6d} "
        msg += f"{d.atom3.number:>6d} "
        msg += f"{d.atom4.number:>6d} "
        msg += "  3 "
        # python 3.7 dicts are sorted, but just in case!
        coeffs = ["c0", "c1", "c2", "c3", "c4", "c5"]
        for coeff in coeffs:
            c = d.gromacs["param"][coeff]
            msg += f"{c:>12.6f}"

        print(msg, file=self.fp)

    def __print_dih3_TI(self, d1, d2):
        """Helper function print RB dihedrals for TI.
        Requires two DihedralType objects
        """
        msg = f"{d1.atom1.number:>6d} "
        msg += f"{d1.atom2.number:>6d} "
        msg += f"{d1.atom3.number:>6d} "
        msg += f"{d1.atom4.number:>6d} "
        msg += "  3 "
        for d in [d1, d2]:
            coeffs = ["c0", "c1", "c2", "c3", "c4", "c5"]
            for coeff in coeffs:
                c = d.gromacs["param"][coeff]
                msg += f"{c:>12.6f} "
        msg += " ; TI dihedrals"
        print(msg, file=self.fp)

    def __do_dihedrals(self):
        """Mutate dihedrals"""

        map_all = self.map_all
        print("\n [ dihedrals ]", file=self.fp)
        dum = False
        for d_l in self.dihedrals_l:
            if (
                    d_l.atom1.number in self.id_mut
                    or d_l.atom2.number in self.id_mut
                    or d_l.atom3.number in self.id_mut
                    or d_l.atom4.number in self.id_mut
            ):
                id_s = []
                for at_d in [d_l.atom1, d_l.atom2, d_l.atom3, d_l.atom4]:
                    if at_d.number in self.map_all:
                        id_s.append(map_all[at_d.number])
                    else:
                        # must be dummy, but check
                        name_b1 = self.atoms_l[at_d.number - self.str_at].name
                        if name_b1 in self.mut_pairs:
                            if self.mut_pairs[name_b1] == "DUM":
                                dum = True
                            else:
                                logging.warning(
                                    f"{name_b1} not mapped or a dummy"
                                )
                        else:
                            print(f"{name_b1} not mapped or a dummy")
                if dum:
                    func = d_l.gromacs["func"]
                    if func == 3:
                        self.__print_dih3_single(d_l)
                    elif func == 1:
                        self.__print_dih1_single(d_l)
                        pass
                    else:
                        print(
                            f"Oops, unknown function type {func}, only ft. 3 (RB) and ft. 1 are supported"
                        )
                    dum = False
                else:
                    for d_s in self.dihedrals_s:
                        if (
                                id_s[0] == d_s.atom1.number
                                and id_s[1] == d_s.atom2.number
                                and id_s[2] == d_s.atom3.number
                                and id_s[3] == d_s.atom4.number
                        ) or (
                                id_s[3] == d_s.atom1.number
                                and id_s[2] == d_s.atom2.number
                                and id_s[1] == d_s.atom3.number
                                and id_s[0] == d_s.atom4.number
                        ):
                            func = d_l.gromacs["func"]
                            if func == 1:
                                self.__print_dih1_TI(d_l, d_s)
                            elif func == 3:
                                self.__print_dih3_TI(d_l, d_s)
                            else:
                                msg = f"unsuported ft. {func} for dihedrals, "
                                msg += f"only ft. 3 and ft. 1 can be used."
                                logging.error(msg)
                                sys.exit()
            else:
                func = d_l.gromacs["func"]
                if func == 3:
                    self.__print_dih3_single(d_l)
                elif func == 1:
                    self.__print_dih1_single(d_l)
                else:
                    msg = f"unsuported ft. {func} for dihedrals, "
                    msg += f"only ft. 3 and ft. 1 can be used."
                    logging.error(msg)
                    sys.exit()

    def mutate(self):
        with open(self.out_itp, "w") as self.fp:
            self.__prepare_atomtypes()
            logging.debug("Atomtypes written")
            self.__write_moltype()
            logging.debug("Molecule type written")
            self.__mutate_atoms()
            logging.debug("Atoms written")
            self.__do_built_map_all()
            self.__do_pairs()
            logging.debug("Pairs written")
            self.__mutate_bonds()
            logging.debug("Bonds written")
            self.__do_angles()
            logging.debug("Angles written")
            self.__do_dihedrals()
            logging.debug("Dihedrals written")


def validate_mut_type(input_val):
    if input_val not in MUTATION:
        msg = "%s is an invalid argument for -mut_type \n" % input_val
        msg += "Valid options: " + " ".join(list(MUTATION.keys()))
        raise argparse.ArgumentTypeError(msg)
    return input_val


def parse_arguments():
    # input parsing
    parser = argparse.ArgumentParser(description=Description, epilog=whowhen)
    parser.add_argument(
        "-il",
        metavar="--itp_large",
        type=str,
        nargs=1,
        help="An itp file with all ff parameters explicit. Should contain\
                        more atoms than the -is itp file.",
        required=True,
    )
    parser.add_argument(
        "-is",
        metavar="--itp_small",
        type=str,
        nargs=1,
        help="An itp file with all ff parameters explicit. Should contain\
                      less atoms than the -il itp file.",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--out_itp",
        nargs="?",
        type=str,
        default="ti_topology.itp",
        help="Output topology itp file for TI.",
    )
    parser.add_argument(
        "-r",
        metavar="--mut_res_num",
        default=1,
        type=float,
        nargs="+",
        help="Residue number to mutate.",
        required=True,
    )
    parser.add_argument(
        "-mut_type",
        metavar="--mutation_type",
        default="mC2C",
        type=validate_mut_type,
        nargs=1,
        help='Mutation type: "mC2C" or "hmC2mC"',
        required=True,
    )
    return vars(parser.parse_args())


# TODO: make a function that can be called also, accepting the same
# arguments as the command line

def run_mutate():
    """You can call it from outside as a function, via cmdline parsing"""
    args_run = parse_arguments()
    Mutator(args_run).mutate()


def mutate_api(*, ilarge: str = None, ismall: str = None,
               mut_residues: List[int] = None, out_itp: str = None,
               mut_type: str = None):
    """Run via a function call. If you want to be on the safe side, check
    that the keyword arguments are not None, a bit lazy now.
    """
    arguments = {}
    # a bit dumb that we have to put the fnames in an array, but this is
    # for compatibility with the argparse input
    arguments.setdefault("il", [ilarge])
    arguments.setdefault("is", [ismall])
    arguments.setdefault("out_itp", out_itp)
    arguments.setdefault("r", mut_residues)
    arguments.setdefault("mut_type", [mut_type])
    validate_mut_type(mut_type)
    Mutator(arguments).mutate()


if __name__ == "__main__":
    """If run as standalone, via cmdline parsing"""
    args = parse_arguments()
    Mutator(args).mutate()
