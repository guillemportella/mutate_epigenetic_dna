import argparse
import logging
import shutil
import sys
from argparse import RawDescriptionHelpFormatter
from typing import List

import pkg_resources

from mutate_epigenetic_dna.mutate_files import run_mutations
from mutate_epigenetic_dna.parsing import parse_input_file, validate_input

DESC_MAIN = f"""Create a GMX topology file to mutate hmC to mC and/or mC to C

Given a pdb structures of *only DNA* fibers containing hmC, mC or C, prepare a 
topology to mutate from hmC -> mC, or/and mC -> C. 

It uses tleap to create a valid amber topology, and then amb2gmx_dihe.pl 
to create a Gromacs itp file that interpolates between two topologies.

As a user, you need to store each pdb file in a folder named as the epigenetic 
modification that the pdb contains, an a folder with the required force field 
parameters. You must also have a folder containing all the required prep
and frcmod/lib files that tleap needs to create a topology.

External dependencies: tleap (actually ambertools), amb2gmx_dihe.pl and 
rdparm (which should come with ambertools).

Make sure to have amb2gmx_dihe.pl script in the path, which requires 
perl and rdparm. The script can be found in the amb2gmx_script folder
of the package source code.

To use it, create an input file with contents like these ones, 

[mutations]
mutations = hmC_mC mC_C
mut_resid = 7 19
[pdb]
hmC = hmC/2mb7_hmC.pdb
mC = mC/2mb7_mC.pdb
C = C/2mb7_C.pdb
[topologies]
ff_folder = ff_amb
tleap_header = tleap_header.inp
hmC = DH.prep
mC = DM.prep
C = DC.prep


(note: run the script with -example to have this example file written out)

Possible improvements TODO: read the atom transformation mapping from a 
file instead of having them hardcoded. Reading in an json file would do the
trick.

"""

DATE = "06-2019"

version = pkg_resources.require("mutate_epigenetic_dna")[0].version
whowhen = f"Guillem Portella, v{version} {DATE}"

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

ALLOWED_MUTATIONS = ["hmC_mC", "mC_C"]

REQUIRED_EXTERNAL = ['tleap', 'amb2gmx_dihe.pl']


def write_example_inp():
    with open("mutate_example.ini", "w") as fo:
        txt = """[mutations]
mutations = hmC_mC mC_C
mut_resid = 7 19
[pdb]
hmC = hmC/2mb7_hmC.pdb
mC = mC/2mb7_mC.pdb
C = C/2mb7_C.pdb
[topologies]
ff_folder = ff_amb
tleap_header = tleap_header.inp
hmC = DH.prep
mC = DM.prep
C = DC.prep
"""
        print(txt, file=fo)
        print("Check out mutate_example.ini")


def check_cmds_in_path(required_external: List):
    """Make sure that the required external commands can be executed """

    for cmd in required_external:
        if shutil.which(cmd) is None:
            msg = f"command {cmd} not in " \
                f"the path"
            logging.error(msg)
            sys.exit()


def parse_cmd_line():
    """Parse the arguments.

    rtype: arguments dictionary
    """
    # create the top-level parser
    parser = \
        argparse.ArgumentParser(description=DESC_MAIN,
                                epilog=whowhen,
                                formatter_class=RawDescriptionHelpFormatter)
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-example", action="store_true", help="Write an example input"
    )
    group.add_argument(
        "-inp",
        metavar="input_file",
        nargs="+",
        help="Input file preparing TI mutations",
    )

    if not len(sys.argv) > 1:
        parser.parse_args(["--help"])

    args = parser.parse_args()

    if args.example:
        write_example_inp()
        sys.exit()

    parsed = parse_input_file(args.inp[0], verbose=False)
    validate_input(parsed)
    check_cmds_in_path(REQUIRED_EXTERNAL)
    run_mutations(parsed)
