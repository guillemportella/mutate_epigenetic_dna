import configparser
import logging
import sys
from dataclasses import dataclass
from functools import partial
from pathlib import Path
from typing import Dict, List, Set, Tuple

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

ALLOWED_MUTATIONS = ["hmC_mC", "mC_C"]


@dataclass
class Input:
    mutations: List[str]
    pdb: Dict[str, Path]
    ff_folder: Path
    tleap_header: Path
    mut_resid: List[int]
    prep_files: Dict[str, Path]


def read_config(cf: configparser.ConfigParser, section: str, opt: str) -> str:
    """Return the value of a configuration"""
    if cf.has_section(section):
        if cf.has_option(section, opt):
            return cf.get(section, opt)
        else:
            logging.error(f"Could not find option {section}/{opt}")
            sys.exit()
    else:
        logging.error(f"Hei! I Could not find section {section}")
        sys.exit()


def parse_mutations(config: configparser.ConfigParser) -> Tuple[
    List[str], List[int], Set[str]]:
    """Parse the mutation input"""
    mut_reader = partial(read_config, config, "mutations")
    mutations = mut_reader("mutations")
    mut_residues = [int(x) for x in mut_reader("mut_resid").split()]
    expected_pdb = set()
    for mut in mutations.split():
        if mut not in ALLOWED_MUTATIONS:
            logging.error(f"I did not understand mutation {mut}")
            logging.error(f"Allowed mutations: {' '.join(ALLOWED_MUTATIONS)}")
            sys.exit()
        else:
            for entity in mut.split("_"):
                expected_pdb.add(entity)
    return mutations.split(), mut_residues, expected_pdb


def parse_pdbs(config: configparser.ConfigParser, expected: Set[str]) -> Dict[
    str, Path]:
    """Get the required pdb files"""
    pdb_reader = partial(read_config, config, "pdb")
    dict_pdb = {}
    for pdb in expected:
        pdb_f = Path(pdb_reader(pdb))
        dict_pdb.setdefault(pdb, pdb_f)
    return dict_pdb


def parse_topologies(config: configparser.ConfigParser,
                     expected: Set[str]) -> Tuple[Path, Path, Dict[str, Path]]:
    """Get the force field information"""

    top_reader = partial(read_config, config, "topologies")
    ff_folder = Path(top_reader("ff_folder"))
    tleap_header = Path(top_reader("tleap_header"))
    dict_prep = {}
    for pdb in expected:
        prep = Path(top_reader(pdb))
        dict_prep.setdefault(pdb, prep)
    return ff_folder, tleap_header, dict_prep


def parse_input_file(inp_f: str, verbose: bool) -> Input:
    """Read in input file.
    """
    config = configparser.ConfigParser()
    config.read(inp_f)
    mutations, residues, expected_pdb = parse_mutations(config)
    dict_pdbs = parse_pdbs(config, expected_pdb)
    ff_folder, tleap_header, dict_prep = parse_topologies(config, expected_pdb)

    parsed_input = Input(mutations=mutations,
                         pdb=dict_pdbs,
                         ff_folder=ff_folder,
                         tleap_header=tleap_header,
                         prep_files=dict_prep,
                         mut_resid=residues,
                         )
    return parsed_input


def validate_input(inp: Input):
    """Make sure files are accessible"""

    if not inp.ff_folder.is_dir():
        logging.error(f"{str(inp.ff_folder)} is not a valid folder")
        sys.exit()

    for f in inp.pdb.values():
        if not f.is_file():
            logging.error(f"{str(f)} PDB file can not be found")

    for f, v in inp.prep_files.items():
        p = inp.ff_folder / v
        if not p.is_file():
            logging.error(f"{str(p)} is not a valid force field folder")
