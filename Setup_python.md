
# Set up a python3 enviroment using conda

If you don't have a python3 environment, as opposed to a python3 installation that comes with the OS, follow this explanation or get this script for [bash](extras/setup_conda_bash.sh) or [zsh](extras/setup_conda_zsh.sh) and run it in the shell. I have used the `zsh` one a lot, everytime I setup a new machine, but the `bash` one should work as well. The script will ask if you want to install not only `python3` and the `conda` tools, but a few useful libraries. It's not totally fail proof, but should be trivial to fix if there are errors due to differences in OSes. To load the environments, read the sections at the end of this markdown.

### Set up the `conda` distribution 

I highly recommend you to check out the `miniconda` distribution. It's a convenient way to manage python 3 (aka *modern* python, version 2 will be obsolete by the end of this year). Besides packaging the python interpreter, it has a nice tooling system to install and manage environments and libraries for the python ecosystem. 

Here is a [link](https://docs.conda.io/en/latest/miniconda.html) to the `miniconda` installation instructions. Get the 64 bit installer for python 3.7 and your OS, and follow the instructions on how to run it.

I would then create a `conda` environment for python 3.7, to keep it all  nicely isolated and not collide with any version of python that runs in your mac. For instance, if you have installed `conda` correctly, you would do

```bash
conda create -n py37 python=3.7
```

to create an environment, and then you would load it using

```bash
source activate py37
```

### Check that you've loaded the right binaries in the path

After that loading the environment loaded, check that if you type in the bash shell, 

```bash
which python
```

you get something like 

`/Users/JaneDone/miniconda3/envs/py37/bin/python` 

(assuming here that your username is *JaneDoe*)

Also, check that if you type now 

```bash
python --version
```

it returns you something like `Python 3.7.1` (or newer). 

If not, you might have a shell PATH problem. Make sure that you have something like 

`export PATH="/Users/JaneDoe/miniconda3/bin:$PATH"`


 as the first line in your `~/.bashrc` file, and that you've sourced that file (i.e. `source ~/.bashrc`, in the bash shell ). If you use `zsh`, add it to `.zshrc` and source it. If any of that does not work, I can help you fix that later on, don't go on with the rest of these instructions if the above commands don't work as expected.

#### Install typical dependencies for data-related projects

If, after all that, you have succeded in setting the environment, you now should be able to install python libraries and other programs in an isolated fashion, without creating conflicts. 

For example, this is my very basic and minimal installation that I always set up to get going,

```bash
# remember to source activate py37
pip install numpy
pip install jupyter
# or pip install jupyterlab if you want, as well, 
# which is more modern take on ipython notebooks, 
# and is fully compatible with the file I sent you
pip install scipy
pip install pandas
pip install sklearn
pip install matplotlib
```

You can change `pip` for `conda` in the lines above, same thing. These commands like take care to install the libraries, and their associated dependencies. `Numpy` is for example, super powerfull for numerical calculations, e.g. matrices and vectorized operations, and is the foundation for `pandas` (dataframes, beats excel if you ask me) and `sklearn` (fitting, clustering, machine learning, etc).  `matplotlib` is one very solid option for plotting, but honestly `ggplot` is nicer. If you like `ggplot` the closest would be `altair`.


### Load `jupyter` to run notebooks

Simply by

```bash
# remember, after source activate py37
jupyter notebook 
```

This should open a browser, and present you the visual interface of the notebook. You can execute each `cell` by pressing `shift+return` when the cell is active (click a cell with the mouse to activate select them). 

