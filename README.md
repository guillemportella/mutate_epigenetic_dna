
# Create a GMX topology file to mutate hmC to mC and/or mC to C

Given a pdb structures of *only DNA* fibers containing hmC, mC or C, prepare a 
topology to mutate from hmC -> mC, or/and mC -> C. 

It uses `tleap` to create a valid amber topology, and then `amb2gmx_dihe.pl`
to create a Gromacs itp file that interpolates between two topologies.

As a user, you need to store each pdb file in a folder named as the epigenetic 
modification that the pdb contains, an a folder with the required force field 
parameters. You must also have a folder containing all the required prep
and frcmod/lib files that tleap needs to create a topology.

External dependencies: `tleap` (actually ambertools), `amb2gmx_dihe.pl` and 
`rdparm` (which should come with ambertools).

> To reiterate: make sure to have `amb2gmx_dihe.pl` script in the `PATH`, which in turn requires 
`perl` and `rdparm`. The script can be found in the `extras` folder
of this source code.


## Install 

Besides the tools named above, you need to have `python 3` in the path. I recommend setting up a `conda` environment to isolate the installation. See [Setup_python.md](Setup_python.md) for an explanation and a script that should do the job for you.

First of all, clone this repository and `cd` to it, 

```bash 
git clone git@gitlab.com:guillemportella/mutate_epigenetic_dna.git
cd mutate_epigenetic_dna
```

I used the python package `poetry` to set up this tool, so you should install it first. Here's a 
rundown on how to install the package. (Remember, check [Setup_python.md](Setup_python.md) for an explanation on how to setup python).

```bash
# if you don't have it, install poetry
pip install --upgrade poetry
# make sure you have the latest pip
poetry run pip install --upgrade pip
# build the package
poetry build
# install it from the wheel
pip install --upgrade dist/mutate_epigenetic_dna-0.1.1-py3-none-any.whl

# note: poetry also generate a compress folder in dist/ with the whole 
# package and a setup.py file, if you prefere to install from there.
```


#### Tests

There are a few tests I wrote in the folder `tests`. If you have `pytest` installed (`pip install pytest`), and happen to have `ambertools12` like I do, you can run some basic tests like

```bash 
python -m pytest
```

from the root of the package. You will find the files required for testing, as an example input, in 
`test/tests/test_execution`. You can take a look to see which files are typically required. Adapt the 
`tleap_header.inp` file to suit your local configuration of `ambertools` and its force fields.

## Usage 
To use this tool, create an input file containing these fields with the names and options that suit your needs,

```ini
[mutations]
mutations = hmC_mC mC_C
mut_resid = 7 19
[pdb]
hmC = hmC/2mb7_hmC.pdb
mC = mC/2mb7_mC.pdb
C = C/2mb7_C.pdb
[topologies]
ff_folder = ff_amb
tleap_header = tleap_header.inp
hmC = DH.prep
mC = DM.prep
C = DC.prep
```

It should be self explanatory what the fields mean. In this case it will mutate residues 7 and 19, and create two topologies: one for `hmC` to `mC`, and one for `mC` to `C`. In the `[topologies]` section, point to a folder containig all the `amber` topology related files. In this example, in a folder called `ff_amb`, you would place all the `prep`, `frcmod` and required files to run `tleap`. Have a file in there, given in the `tleap_header` field of the input, with the required libraries that `tleap` would need, such as sourcing the right forcefield. There is no need to add the `prep` files in that `tleap_header` file, the tool will add them in the input for you. Take a look at the test inputs I have placed in the repository as example, refer to what I have explained in the section above about testing. 


> **Tip**: run the script with -example to have this example file written out, i.e.: `med -example`

If all the files are in place, and your input is called `input.ini`, then you
can execute it with

```bash
med -inp input.ini
```

#### Using only the code that does the mutation on pre-generated gromacs `itp` files

If you prefer to do it yourself, from a pair of Gromacs `itp` files generated
from `amb2gmx_dihe.pl`, you can check out the utility `med_single`, 

```bash
med_single -h
```

## Usage as a library

If you would want, this tool can be called as a `python` package, and you can access all the functions from your `python` script. E.g., you can import functions from it:

```python
#! /user/bin/env python
import re
import sys

from mutate_epigenetic_dna import start_mutations

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(start_mutations())

```

But the library is not documented, so unless you want to read the code to know what to call, don't do it.


### Possible improvements / TODO

Read the atom transformation mapping from a file instead of having them hardcoded. Reading in a `json` file would do the trick.
