import pathlib
from pathlib import PosixPath

# from mutate_epigenetic_dna.cmdline import Input
from mutate_epigenetic_dna.cmdline import parse_input_file
from mutate_epigenetic_dna.parsing import Input

expected_parsed = Input(mutations=['hmC_mC', 'mC_C'],
                        pdb={'mC': PosixPath('mC/2mb7_mC.pdb'),
                             'hmC': PosixPath('hmC/2mb7_hmC.pdb'),
                             'C': PosixPath('C/2mb7_C.pdb')},
                        ff_folder=PosixPath('ff_amb'),
                        tleap_header=PosixPath('tleap_header.inp'),
                        mut_resid=[7, 19],
                        prep_files={'mC': PosixPath('DM.prep'),
                                    'hmC': PosixPath('DH.prep'),
                                    'C': PosixPath('DC.prep')})

expected_files_test = ["input.ini", "ff_amb", "hmC", "mC", "C"]


def test_data_dir(datadir):
    found_test_files = []
    for f in pathlib.Path(datadir).iterdir():
        found_test_files.append(f.name)

    assert len(expected_files_test) == len(found_test_files) and \
           sorted(found_test_files) == sorted(expected_files_test)


def test_parse_input(datadir):
    input_file = datadir + "/input.ini"
    options_parsed = parse_input_file(input_file, verbose=False)
    print(options_parsed)
    assert expected_parsed == options_parsed
