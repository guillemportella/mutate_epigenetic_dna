import os
import tempfile
from distutils import dir_util

import pytest


@pytest.fixture()
def datadir(tmpdir, request):
    '''
    Fixture responsible for searching a folder with the same name of test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.
    '''
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)

    if os.path.isdir(test_dir):
        with tempfile.TemporaryDirectory() as temp_d:
            dir_util.copy_tree(test_dir, temp_d)

            yield temp_d
